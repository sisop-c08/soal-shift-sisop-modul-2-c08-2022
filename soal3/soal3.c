#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <regex.h>
#include <string.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void unzip(char *zipFile, char *dest) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"unzip", "-d", dest, zipFile, NULL};
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
	}
}


int findPattern(char *str, char *pat) 
{
	return (strstr(str, pat) != NULL);
}


void move(char *src, char *fileName, char *dest) 
{
	char source[100];
	strcpy(source, src);
	strcat(source, fileName);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeDir(char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		printf("%s folder deleted\n", dir);

		char *argv[] = {"rm", "-r", dir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeFile(char *dir, char *fileName) 
{
	char source[100];
	strcpy(source, dir);
	strcat(source, fileName);

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", source, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

const char* getDetails(char *dir, char *fileName) 
{
	char *res;
	res = malloc(sizeof(char)*100);
	int r, s;
	
	char filePath[100];
	strcpy(filePath, dir);
	strcat(filePath, fileName);
	
	struct stat info;
	r = stat(filePath, &info);
		
	struct passwd *pw = getpwuid(info.st_uid);
	
	if (pw != 0) strcpy(res, pw->pw_name);
	
	strcat(res, "_");
	
	struct stat fs;
	s = stat(filePath, &fs);

	if (fs.st_mode & S_IRUSR) 
		strcat(res, "r");
	if (fs.st_mode & S_IWUSR) 
		strcat(res, "w");
	if (fs.st_mode & S_IXUSR) 
		strcat(res, "x");
		
	strcat(res, "_");
	
	strcat(res, fileName);
	
	return res;
}

void writeToTxt(char *fileName, char *dest) 
{
	char filePath[100];
	strcpy(filePath, dest);
	strcat(filePath, fileName);

	FILE *fp = fopen(filePath, "w");
	
	if (fp == NULL)
		printf("error opening file\n");
	
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(dest);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "jpg") || 
    		findPattern(dirp->d_name, "png"))
	    	fprintf(fp, "%s\n", getDetails(dest, dirp->d_name));
    }
    closedir(dp);
    
    fclose(fp);
}

int main() 
{
	char* pathDarat = "/home/doanda/modul2/darat/";
	char* pathAir = "/home/doanda/modul2/air/";
	char* pathAnimal = "/home/doanda/modul2/animal/";
	char* pathModul = "/home/doanda/modul2/";
	char* zipFile = "animal.zip";
	char* textFile = "list.txt";
	
	makeDir(pathDarat);
	sleep(3);
	makeDir(pathAir);
	
	unzip(zipFile, pathModul);
	
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "darat")) {
    		move(pathAnimal, dirp->d_name, pathDarat);
    	}
    }
    closedir(dp);
    
    sleep(3);

    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "air"))
    		move(pathAnimal, dirp->d_name, pathAir);
    }
    closedir(dp);
    
    removeDir(pathAnimal);
    
    dp = opendir(pathDarat);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "bird"))
    		removeFile(pathDarat, dirp->d_name);
    }
    closedir(dp);
    
    writeToTxt(textFile, pathAir);
    
	return 0;
}
