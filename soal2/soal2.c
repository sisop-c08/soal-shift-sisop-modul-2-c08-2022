#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

void unzip(char *zip, char *target) {
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"unzip", zip, "-d", target, NULL};
        execv("/usr/bin/unzip", argv);
        
        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void removeFiles(char *name, int type) {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        if(type == 0){
            char *argv[] = {"rm", "-rf", name, NULL};
            execv("/bin/rm", argv);
        }else if(type == 1){
            char *argv[] = {"rm", name, NULL};
            execv("/bin/rm", argv);
        }

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}

void makeTxtFile(char *category, char *name, char *year) {
    char file[50];
    sprintf(file, "%s/data.txt", category);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *writef;
        writef = fopen(file, "a");

        fprintf(writef, "%s;%s\n", name, year);

        fclose(writef);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void sortTxtFile() {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"sh", "-c", "sort -t';' -k2 data.txt > sorted.txt", NULL};
        execv("/bin/sh", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}

void refactorTxtFile(char *name){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *readf, *writef;
        readf = fopen("sorted.txt", "r");
        writef = fopen("data.txt", "w");

        fprintf(writef, "kategori = %s\n", name);

        char line[100];

        while(fgets(line, sizeof(line), readf)){
                char *breaks = strtok(line, ";");
                fprintf(writef,"\nnama = %s", breaks);

                breaks = strtok(NULL, ";");
                fprintf(writef,"\nrilis = tahun %s", breaks);
        }

        fclose(readf);
        fclose(writef);

        removeFiles("sorted.txt", 1);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void copyFile(char *name_old, char *name_new, char *folder) {
    char source[50];
    sprintf(source, "%s", name_old);

    char target[50];
    sprintf(target, "%s/%s.png", folder, name_new);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"cp", source, target, NULL};
        execv("/bin/cp", argv);

        exit(EXIT_SUCCESS); 
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void createFolder(char *name) {
    DIR *dir = opendir(name);

    if (dir) {
        return;
    }

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mkdir", name, NULL};
        execv("/bin/mkdir", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void categorize(char *file) {
    char name[100];
    sprintf(name, "%s", file);

    char *breaks;
    char *data[3];
    breaks = strtok(file, ";_");

    while (breaks != NULL) {
        for (int i = 0; i < 3; ++i) {
            data[i] = breaks;
            breaks = strtok(NULL, ";_");
        }
            
        char *drakor_title = data[0];
        char *drakor_year = data[1];
        char *drakor_category = data[2];
		
        char *category;
        category = strstr(drakor_category, ".png");

        if (category != NULL) {
            int pos = category - drakor_category;
            sprintf(drakor_category, "%.*s", pos, drakor_category);
        }

        createFolder(drakor_category);
        copyFile(name, drakor_title, drakor_category);
        makeTxtFile(drakor_category, drakor_title, drakor_year);
    }
   
}

int main() {
    char path[] = "/home/dhika/shift2/";
    char zip[] = "drakor.zip";
    char folder[] = "drakor";

    chdir(path);
    	
    unzip(zip, folder);
    	
    struct dirent *dp;
    DIR *dir = opendir(folder);
    
    if (!dir) {
	    exit(EXIT_FAILURE);
    }
	
    chdir(folder);
	
    while (dp = readdir(dir)) {
	    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            if(dp->d_type == DT_DIR){
                removeFiles(dp->d_name, 0);
            }else{
                categorize(dp->d_name);
            }
        }
    }

    chdir(path);

    dir = opendir(folder);
    
	if (!dir) {
		exit(EXIT_FAILURE);
	}

    chdir(folder);

    while (dp = readdir(dir)) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			if(dp->d_type == DT_REG){
                removeFiles(dp->d_name, 1);
            } else{
                chdir(dp->d_name);
                sortTxtFile();
                refactorTxtFile(dp->d_name);
                chdir(path);
                chdir(folder);
            }
        }
    }
    	
	return 0;
}
