# Laporan Praktikum Modul 2
# Identitas Kelompok
### Kelompok C08
 1.  __Doanda Dresta Rahma	5025201049__
 2.  __Putu Andhika Pratama	5025201132__
 3.  __Muhammad Raihan Athallah	5025201206__

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)


# Soal 1
Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek.
Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu
ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena
rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program
untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih
suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan
program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi
keinginan nya itu.

## A. Download Unzip Delete Mkdir

Declare variabel yang akan digunakan untuk fungsi exec sesuai dengan perintahnya masing-masing seperti __wget__ untuk download, __unzip__ untuk unzip, __mkdir__ untuk membuat directory baru dan __rm__ untuk remove file. Setelah melakukan declare panggil fungsi sesuai dengan fungsinya masing-masing.
```c
int main(){

    char *argsdownload1[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-O","char.zip",NULL};
    char *argsdownload2[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-O", "weapons.zip",NULL};
    char *argsunzip1[] = {"unzip","char.zip",NULL};
    char *argsunzip2[] = {"unzip","weapons.zip",NULL};
    char *argsrmv1[] = {"rm","char.zip",NULL};
    char *argsrmv2[] = {"rm","weapons.zip",NULL};
    char *argmkdir[] = {"mkdir", "-p", "gacha_gacha",NULL};

    //download(argsdownload1);
    printf("char downloaded\n");
    //download(argsdownload2);
    printf("weapons downloaded\n");

    sleep(1);

    unzip(argsunzip1);
    printf("unzip char\n");
    unzip(argsunzip2);
    printf("unzip weapons\n");
    
    sleep(1);
    makedir(argmkdir);
    printf("makedir gacha\n");
    
    gacha();
    
    system("zip -r --password pass not_safe_for_wibu.zip gacha_gacha/*");
    
    char *dirName = "gacha_gacha";
    char *password = "satuduatiga";
    char *zipName = "not_safe_for_wibu.zip";
```

### Fungsi Download
```c
void download(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/wget", command);
    } 
    else{
		while(wait(&status) > 0);
    }
}
```
### Fungsi Mkdir
```c
void makedir(char *command[])
{
    pid_t child_id;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/mkdir", command);
    } 
    else{
    int status;
		while(wait(&status) > 0);
    }
    
}
```
### Fungsi Mkdir Versi 2
```c
void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
### Fungsi Unzip
```c
void unzip(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();

	if (child_id == 0) {
		execv("/bin/unzip", command);
    }
    else {
		while(wait(&status) > 0);
    }
}
```

### Fungsi Zip
```c
void zip(char *name, char *password) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
    	char zipName[100];
    	sprintf(zipName, "%s.zip %s/", name, name);
		char *argv[] = {"zip", "--password", password, "-r", zipName, NULL};

    	execv("/bin/zip", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}
```

### Fungsi Rename
```c
void mvRename(char *oldName, char *newName) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
		char *argv[] = {"mv", oldName, newName, NULL};
		execv("/bin/mv", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}
```

### Fungsi gettime
```c
char* getTime() 
{
	int h, m, s;
	char *curtime = malloc(sizeof(char)*50);

	time_t now = time(NULL);
	struct tm *local = localtime(&now);

	h = local->tm_hour;
	m = local->tm_min;
	s = local->tm_sec;

	sprintf(curtime, "%02d:%02d:%02d", h, m, s);
	
	return curtime;
}
```
### Fungsi Print txt
```c
void outputTxt(char *dir, int *primo, int *count) 
{
	// get current time in hh:mm:ss format
	char curtime[50];
	strcpy(curtime, getTime());

	// open txt (dummy name)
	char dummyPath[100];
	sprintf(dummyPath, "%s/dummy", dir);

	FILE *fp = fopen(dummyPath, "w");

	if (fp == NULL)
		printf("error opening file\n");

	for (int i = 0; i < 10; i++) 
	{
		if (*primo < 160) break;

		char hasilGacha[100];
		*count += 1;
		*primo -= 160;

		if (i % 2 == 0) {
			char *charData = pullChar(); // get string rarity_name
			sprintf(hasilGacha, "%d_characters_%s_%d", *count, charData, *primo);
		}
		else if (i % 2 != 0) {
			char *weaponData = pullWeapon(); // get string rarity_name
			sprintf(hasilGacha, "%d_weapons_%s_%d", *count, weaponData, *primo);
		}
		
		fprintf(fp, "%s\n", hasilGacha);
	}

	fclose(fp);
	
	// rename txt file
	char filePath[100];
	sprintf(filePath, "%s/%s_gacha_%d", dir, curtime, *count);
	mvRename(dummyPath, filePath);
}
```
### Fungsi Gacha
```c
void gacha() {
	int primogems = 20000;
	int gcount = 0;
	
	char dummyPath[50] = "gacha_gacha/dummy";
	char folderPath[50];
	
	while (primogems >= 160) {
		if (gcount % 9 == 0) {
			if (gcount != 0) {
				sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
				mvRename(dummyPath, folderPath);
			}
			makeDir(dummyPath); // wait
		}
			
		outputTxt(dummyPath, &primogems, &gcount);
		sleep(1);
	}

	sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
	mvRename(dummyPath, folderPath);
}
```
## Kesulitan Soal 1
kesulitan yang dihadapi kesusahan dalam mencari fungsi alternatif system()

## Source Code Soal 1
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>

void download(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/wget", command);
    } 
    else{
		while(wait(&status) > 0);
    }
}

void makedir(char *command[])
{
    pid_t child_id;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/mkdir", command);
    } 
    else{
    int status;
		while(wait(&status) > 0);
    }
    
}

// makedir versi 2
void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void unzip(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();

	if (child_id == 0) {
		execv("/bin/unzip", command);
    }
    else {
		while(wait(&status) > 0);
    }
}

void remove_file(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/rm", command);
    } 
    else {
		while(wait(&status) > 0);
    }
}

void zip(char *name, char *password) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
    	char zipName[100];
    	sprintf(zipName, "%s.zip %s/", name, name);
		char *argv[] = {"zip", "--password", password, "-r", zipName, NULL};

    	execv("/bin/zip", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}

void mvRename(char *oldName, char *newName) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
		char *argv[] = {"mv", oldName, newName, NULL};
		execv("/bin/mv", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}

char* getTime() 
{
	int h, m, s;
	char *curtime = malloc(sizeof(char)*50);

	time_t now = time(NULL);
	struct tm *local = localtime(&now);

	h = local->tm_hour;
	m = local->tm_min;
	s = local->tm_sec;

	sprintf(curtime, "%02d:%02d:%02d", h, m, s);
	
	return curtime;
}

// stub
char* pullChar() 
{
	return "5_childe";
}

// stub
char* pullWeapon() 
{
	return "5_bigboysword";
}

void outputTxt(char *dir, int *primo, int *count) 
{
	// get current time in hh:mm:ss format
	char curtime[50];
	strcpy(curtime, getTime());

	// open txt (dummy name)
	char dummyPath[100];
	sprintf(dummyPath, "%s/dummy", dir);

	FILE *fp = fopen(dummyPath, "w");

	if (fp == NULL)
		printf("error opening file\n");

	for (int i = 0; i < 10; i++) 
	{
		if (*primo < 160) break;

		char hasilGacha[100];
		*count += 1;
		*primo -= 160;

		if (i % 2 == 0) {
			char *charData = pullChar(); // get string rarity_name
			sprintf(hasilGacha, "%d_characters_%s_%d", *count, charData, *primo);
		}
		else if (i % 2 != 0) {
			char *weaponData = pullWeapon(); // get string rarity_name
			sprintf(hasilGacha, "%d_weapons_%s_%d", *count, weaponData, *primo);
		}
		
		fprintf(fp, "%s\n", hasilGacha);
	}

	fclose(fp);
	
	// rename txt file
	char filePath[100];
	sprintf(filePath, "%s/%s_gacha_%d", dir, curtime, *count);
	mvRename(dummyPath, filePath);
}

void gacha() {
	int primogems = 20000;
	int gcount = 0;
	
	char dummyPath[50] = "gacha_gacha/dummy";
	char folderPath[50];
	
	while (primogems >= 160) {
		if (gcount % 9 == 0) {
			if (gcount != 0) {
				sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
				mvRename(dummyPath, folderPath);
			}
			makeDir(dummyPath); // wait
		}
			
		outputTxt(dummyPath, &primogems, &gcount);
		sleep(1);
	}

	sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
	mvRename(dummyPath, folderPath);
}


int main(){

    char *argsdownload1[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-O","char.zip",NULL};
    char *argsdownload2[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-O", "weapons.zip",NULL};
    char *argsunzip1[] = {"unzip","char.zip",NULL};
    char *argsunzip2[] = {"unzip","weapons.zip",NULL};
    char *argsrmv1[] = {"rm","char.zip",NULL};
    char *argsrmv2[] = {"rm","weapons.zip",NULL};
    char *argmkdir[] = {"mkdir", "-p", "gacha_gacha",NULL};

    //download(argsdownload1);
    printf("char downloaded\n");
    //download(argsdownload2);
    printf("weapons downloaded\n");

    sleep(1);

    unzip(argsunzip1);
    printf("unzip char\n");
    unzip(argsunzip2);
    printf("unzip weapons\n");
    
    sleep(1);
    makedir(argmkdir);
    printf("makedir gacha\n");
    
	// gacha
    gacha();
    
    // zip gacha_gacha "not_safe_for_wibu", masi pake system
    system("zip -r --password pass not_safe_for_wibu.zip gacha_gacha/*");
    
    // versi bukan system, tapi masi eror
    char *dirName = "gacha_gacha";
    char *password = "satuduatiga";
    char *zipName = "not_safe_for_wibu.zip";
    
    return 0;

}

```
# Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat
ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea
yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah
mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review,
tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan
untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun
untuk menyelesaikan pekerjaannya.

a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang
diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun
teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka
program harus bisa membedakan file dan folder sehingga dapat memproses file yang
seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus
membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu
tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan
folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis
drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder
dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di
pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama
“start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder
“/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.

e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama
dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting
list serial di file ini berdasarkan tahun rilis (Ascending).
## 1. Ekstrak zip
Pada fungsi main memanggil fungsi unzip
```c
    char path[] = "/home/dhika/shift2/";
    char zip[] = "drakor.zip";
    char folder[] = "drakor";

    chdir(path);
    	
    unzip(zip, folder);
```
Pada fungsi unzip membuat child process dengan argumen unzip -d, dimana file zip akan diunzip kemudian di letakkan pada folder target. Parent process akan menunggu hingga child process selesai.
```c
void unzip(char *zip, char *target) {
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"unzip", zip, "-d", target, NULL};
        execv("/usr/bin/unzip", argv);
        
        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}
```
## 2. Directory Listing pada folder drakor
Membuat struct dirent dp untuk membaca directory dir, dir akan membuka folder drakor, jika tidak terdapat folder drakor maka program berhenti.
```c
    struct dirent *dp;
    DIR *dir = opendir(folder);
    
    if (!dir) {
      exit(EXIT_FAILURE);
    }
	
    chdir(folder);
```

## 3. Traverse pada folder drakor
Dengan menggunakan looping while, dirent dp akan melakukan traverse pada folder drakor, kemudian menggunakan if untuk mengabaikan folder sebelumnya atau folder root, lalu menggunakan if else untuk mengecek apakah file tersebut merupakan folder atau bukan, jika file tersebut merupakan folder maka folder tersebut akan dihapus, jika file bukan murupakan folder maka akan memanggil fungsi categorize dengan passing variabel name file.
```c
    while (dp = readdir(dir)) {
	      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            if(dp->d_type == DT_DIR){
                removeFiles(dp->d_name, 0);
            }else{
                categorize(dp->d_name);
            }
        }
    }
```
## 4. Remove files
Pada fungsi removeFiles membutuhkan parameter nama file yang kan dihapus dan type file yang akan dihapus (0 berarti folder dan 1 berarti file). Kemudian akan membuat child process dimana pada child process akan menggunakn if else untuk mengecek type file akan akan dihapus. Jika folder(0) maka akan menggunakan argumen rm -rf namafolder. Dan jika file(1) akan menggunakan argumen rm namafile.
```c
void removeFiles(char *name, int type) {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        if(type == 0){
            char *argv[] = {"rm", "-rf", name, NULL};
            execv("/bin/rm", argv);
        }else if(type == 1){
            char *argv[] = {"rm", name, NULL};
            execv("/bin/rm", argv);
        }

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}
```
## 5. Melakukan kategorisasi
Pada fungsi categorisasi terdapat beberapa langkah :
- menginput nama file pada char name
- dengan menggunakan strtok pada variabel breaks, nama file akan dipecah dengan delimiter(pemisah) berupa ';' atau '_'
- karena dalam satu file bisa terdapat lebih dari 1 judul drakor, maka dengan menggunakan while loop jika nilai breaks belum NULL maka proses akan terus dijalankan
- kemudian menggunakan for looping untuk mengulang proses strtok dan menginput string yang sudah dipecah kedalam data 1-3
- ketika proses for loop selesai, data 1-3 dimasukkan kedalam variabel yang sesuai
- menggunakan strtok pada variabel category lagi untuk memecah drakor_category jika terdapat .png pada penamaan, jika penamaan terdapat .png maka drakor_category akan diubah
- memanggil fungsi createFolder (dijelaskan pada poin 6)
- memanggil fungsi copyFile (dijelaskan pada poin 7)
- memanggil fungsi makeTxtFile (dijelaskan pada poin 8)
```c
void categorize(char *file) {
    char name[100];
    sprintf(name, "%s", file);

    char *breaks;
    char *data[3];
    breaks = strtok(file, ";_");

    while (breaks != NULL) {
        for (int i = 0; i < 3; ++i) {
            data[i] = breaks;
            breaks = strtok(NULL, ";_");
        }
            
        char *drakor_title = data[0];
        char *drakor_year = data[1];
        char *drakor_category = data[2];
		
        char *category;
        category = strstr(drakor_category, ".png");

        if (category != NULL) {
            int pos = category - drakor_category;
            sprintf(drakor_category, "%.*s", pos, drakor_category);
        }

        createFolder(drakor_category);
        copyFile(name, drakor_title, drakor_category);
        makeTxtFile(drakor_category, drakor_title, drakor_year);
    }
   
}
```
## 6. Membuat folder kategori
Pada fungsi createFolder membutuhkan parameter nama, dengan menggunakan if akan mengecek apakah folder kategori yang ingin dibuat sudah ada, jika sudah ada maka akan keluar dari fungsi. Kemudian jika belum ada maka akan membuat child process dengan argumen mkdir name(nama folder yang ingin dibuat). Parent process akan menunggu hingga child process selesai.
```c
void createFolder(char *name) {
    DIR *dir = opendir(name);

    if (dir) {
        return;
    }

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mkdir", name, NULL};
        execv("/bin/mkdir", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}
```
## 7. Mengcopy file kedalam folder kategori yang sesuai
Pada fungsi createFolder membutuhkan parameter nama lama, nama baru, dan nama folder tempat file baru akan disimpan. Kemudian membuat char source untuk nama lama dan char target untuk path dan nama baru. Lalu membuat child process dengan argumen cp source(file yang akan dicopy) target(path dan nama file hasil copy). Parent process akan menunggu hingga child process selesai.
```c
void copyFile(char *name_old, char *name_new, char *folder) {
    char source[50];
    sprintf(source, "%s", name_old);

    char target[50];
    sprintf(target, "%s/%s.png", folder, name_new);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"cp", source, target, NULL};
        execv("/bin/cp", argv);

        exit(EXIT_SUCCESS); 
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}
```
## 8. Membuat file txt
Pada fungsi makeTxtFile membutuhkan parameter kategori, nama(judul film), tahun. Membuat char file untuk path yang sesuai, dimana pathnya berupa namakategoryi/data.txt. Lalu membuat child process, dimana pada child process akan membuat variabel file yaitu writef, lalu writef akan membuka char file(sesuai dengan path) dengan mode "a" yaitu append. lalu menggunakan fprintf untuk mencetak name;year pada file.

note : format name;year agar mudah ketika di sorting
```c
void makeTxtFile(char *category, char *name, char *year) {
    char file[50];
    sprintf(file, "%s/data.txt", category);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *writef;
        writef = fopen(file, "a");

        fprintf(writef, "%s;%s\n", name, year);

        fclose(writef);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}
```
## 9. Traverse pada folder drakor lagi
Pindah ke path, lalu var dir membuka folder drakor lagi, jika tidak terdapat folder drakor maka program berhenti. Masuk ke folder drakor. dengan menggunakan looping while, dirent dp akan melakukan traverse pada folder drakor, kemudian menggunakan if untuk mengabaikan folder sebelumnya atau folder root, lalu menggunakan if else untuk mengecek apakah file tersebut merupakan file regular atau bukan, jika file tersebut merupakan file regular maka file tersebut akan dihapus, jika file bukan murupakan file regular maka akan masuk ke folder yang dicek, lalu memanggil fungsi sortTxtFile (dijelaskan pada poin 10) dan fungsi refactorTxtFile (dijelaskan pada poin 11), kemudian pindah kembali ke folder drakor.
```c
    chdir(path);

    dir = opendir(folder);
    
	  if (!dir) {
		    exit(EXIT_FAILURE);
	  } 

    chdir(folder);

    while (dp = readdir(dir)) {
		    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			      if(dp->d_type == DT_REG){
                removeFiles(dp->d_name, 1);
            } else{
                chdir(dp->d_name);
                sortTxtFile();
                refactorTxtFile(dp->d_name);
                chdir(path);
                chdir(folder);
            }
        }
    }
```
## 10. Sorting pada txt file
Pada proses sortTxtFile akan membuat child process dengan argumen:
- sh -c sort (untuk melakukan eksekusi pada sorting)
- -t';' (dengan pemisah(delimiter) berupa ';')
- -k2 (pada kolom kedua)
- data.txt > sorted.txt (pada file data.txt dan hasilnya akan dimasukkan ke sorted.txt)
```c
void sortTxtFile() {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"sh", "-c", "sort -t';' -k2 data.txt > sorted.txt", NULL};
        execv("/bin/sh", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}
```
## 11. Merapikan format pada txt file
Pada fungsi refactorTxtFile terdapat beberapa langkah:
- membutuhkan parameter nama folder
- membuat child process
- pada child process akan membuat variabel file readf(akan membaca sorted.txt) dan writef(akan menulis baru di data.txt)
- diawali dengan fprintf untuk print nama kategori dalam file writef
- lalu dengan while loop akan membaca setiap baris pada file readf
- kemudian akan di print ke dalam writef sesuai dengan format soal
- ketika proses while loop telah selesai, maka file sorted.txt akan di hapus
```c
void refactorTxtFile(char *name){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *readf, *writef;
        readf = fopen("sorted.txt", "r");
        writef = fopen("data.txt", "w");

        fprintf(writef, "kategori = %s\n", name);

        char line[100];

        while(fgets(line, sizeof(line), readf)){
                char *breaks = strtok(line, ";");
                fprintf(writef,"\nnama = %s", breaks);

                breaks = strtok(NULL, ";");
                fprintf(writef,"\nrilis = tahun %s", breaks);
        }

        fclose(readf);
        fclose(writef);

        removeFiles("sorted.txt", 1);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}
```
## Hasil Run

![run program](img/2.jpg)
![file](img/2.file.jpg)
![action](img/2.action.jpg)
![comedy](img/2.comedy.jpg)
![fantasy](img/2.fantasy.jpg)
![horro](img/2.horror.jpg)
![romance](img/2.romance.jpg)
![school](img/2.school.jpg)
![thriller](img/2.thriller.jpg)

## Source Code Soal 2
```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

void unzip(char *zip, char *target) {
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"unzip", zip, "-d", target, NULL};
        execv("/usr/bin/unzip", argv);
        
        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void removeFiles(char *name, int type) {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        if(type == 0){
            char *argv[] = {"rm", "-rf", name, NULL};
            execv("/bin/rm", argv);
        }else if(type == 1){
            char *argv[] = {"rm", name, NULL};
            execv("/bin/rm", argv);
        }

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}

void makeTxtFile(char *category, char *name, char *year) {
    char file[50];
    sprintf(file, "%s/data.txt", category);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *writef;
        writef = fopen(file, "a");

        fprintf(writef, "%s;%s\n", name, year);

        fclose(writef);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void sortTxtFile() {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"sh", "-c", "sort -t';' -k2 data.txt > sorted.txt", NULL};
        execv("/bin/sh", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}

void refactorTxtFile(char *name){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *readf, *writef;
        readf = fopen("sorted.txt", "r");
        writef = fopen("data.txt", "w");

        fprintf(writef, "kategori = %s\n", name);

        char line[100];

        while(fgets(line, sizeof(line), readf)){
                char *breaks = strtok(line, ";");
                fprintf(writef,"\nnama = %s", breaks);

                breaks = strtok(NULL, ";");
                fprintf(writef,"\nrilis = tahun %s", breaks);
        }

        fclose(readf);
        fclose(writef);

        removeFiles("sorted.txt", 1);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void copyFile(char *name_old, char *name_new, char *folder) {
    char source[50];
    sprintf(source, "%s", name_old);

    char target[50];
    sprintf(target, "%s/%s.png", folder, name_new);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"cp", source, target, NULL};
        execv("/bin/cp", argv);

        exit(EXIT_SUCCESS); 
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void createFolder(char *name) {
    DIR *dir = opendir(name);

    if (dir) {
        return;
    }

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mkdir", name, NULL};
        execv("/bin/mkdir", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void categorize(char *file) {
    char name[100];
    sprintf(name, "%s", file);

    char *breaks;
    char *data[3];
    breaks = strtok(file, ";_");

    while (breaks != NULL) {
        for (int i = 0; i < 3; ++i) {
            data[i] = breaks;
            breaks = strtok(NULL, ";_");
        }
            
        char *drakor_title = data[0];
        char *drakor_year = data[1];
        char *drakor_category = data[2];
		
        char *category;
        category = strstr(drakor_category, ".png");

        if (category != NULL) {
            int pos = category - drakor_category;
            sprintf(drakor_category, "%.*s", pos, drakor_category);
        }

        createFolder(drakor_category);
        copyFile(name, drakor_title, drakor_category);
        makeTxtFile(drakor_category, drakor_title, drakor_year);
    }
   
}

int main() {
    char path[] = "/home/dhika/shift2/";
    char zip[] = "drakor.zip";
    char folder[] = "drakor";

    chdir(path);
    	
    unzip(zip, folder);
    	
    struct dirent *dp;
    DIR *dir = opendir(folder);
    
    if (!dir) {
	    exit(EXIT_FAILURE);
    }
	
    chdir(folder);
	
    while (dp = readdir(dir)) {
	    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            if(dp->d_type == DT_DIR){
                removeFiles(dp->d_name, 0);
            }else{
                categorize(dp->d_name);
            }
        }
    }

    chdir(path);

    dir = opendir(folder);
    
	if (!dir) {
		exit(EXIT_FAILURE);
	}

    chdir(folder);

    while (dp = readdir(dir)) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			if(dp->d_type == DT_REG){
                removeFiles(dp->d_name, 1);
            } else{
                chdir(dp->d_name);
                sortTxtFile();
                refactorTxtFile(dp->d_name);
                chdir(path);
                chdir(folder);
            }
        }
    }
    	
	return 0;
}
```

# Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

a.	Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

b.	Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

c.	Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

d.	Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

e.	Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

## A. Membuat 2 Directory
Di fungsi `main` memanggil `makeDir` untuk membuat folder darat, menunggu 3 detik dengan fungsi `sleep` kemudian membuat folder air.

```c
int main() 
{
	char* pathDarat = "/home/doanda/modul2/darat/";
	char* pathAir = "/home/doanda/modul2/air/";
	char* pathAnimal = "/home/doanda/modul2/animal/";
	char* pathModul = "/home/doanda/modul2/";
	char* zipFile = "animal.zip";
	char* textFile = "list.txt";
	
	makeDir(pathDarat);
	sleep(3);
	makeDir(pathAir);
	...
```
### Fungsi `makeDir()`
```c
Menciptakan child process dimana pembuatan folder berlangsung menggunakan `execv`. Parent process menunggu child process selesai. Membutuhkan parameter yaitu directory yang akan dibuat.
void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
## B. Ekstrak Animal.zip
Dalam `main` memanggil `unzip` untuk mengekstrak file zip ke dalam directory animal
```c
	unzip(zipFile, pathModul);
```
### Fungsi `unzip()`
`unzip()` membutuhkan parameter `zipFile` yaitu nama file zip dalam directory sekarang, dan `dest` directory tujuan ekstraksi.
```c
void unzip(char *zipFile, char *dest) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"unzip", "-d", dest, zipFile, NULL};
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
## C. Hewan Darat dan Air di Folder Berbeda, Menghapus Hewan Tanpa Keterangan
Untuk melakukan ini dibutuhkan beberapa aspek: traverse melalui file-file dalam folder, mencocokkan nama file dengan string, memindahkan file ke folder tujuan, dan menghapus folder. Pertama menggunakan dirent untuk membuka folder animal. Kemudian mentraverse folder dengan `readdir()`, mencocokkan nama file dengan pattern 'darat' dengan `findPattern()`, lalu memindahkan file ke folder darat dengan `move()` dan menutup folder animal. Setelah itu menunggu 3 detik. Kemudian mengulangi proses lagi kali ini dengan pattern 'air'. Akhirnya menghapus folder berisi file tersisa dengan `removeDir()`.
```c
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "darat")) {
    		move(pathAnimal, dirp->d_name, pathDarat);
    	}
    }
    closedir(dp);
    
    sleep(3);

    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "air"))
    		move(pathAnimal, dirp->d_name, pathAir);
    }
    closedir(dp);
    
    removeDir(pathAnimal);
```
### Fungsi `findPattern()`
Menggunakan library function `strstr()`.
```c
int findPattern(char *str, char *pat) 
{
	return (strstr(str, pat) != NULL);
}
```
### Fungsi `move()`
Membutuhkan tiga parameter yaitu folder file yang akan dipindah, nama file, dan folder tujuan. src dan fileName digabungkan sebagai salah satu argumen untuk bash command `mv`.
```c
void move(char *src, char *fileName, char *dest) 
{
	char source[100];
	strcpy(source, src);
	strcat(source, fileName);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
### Fungsi `removeDir()`
Fungsi ini menghapus folder beserta isinya menggunakan bash command `rm`.
```c
void removeDir(char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", "-r", dir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
## D. Menghapus Data Burung
Mirip dengan langkah sebelumnya hanya jika nama file match dengan 'bird' file akan dihapus.
```c
    dp = opendir(pathDarat);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "bird"))
    		removeFile(pathDarat, dirp->d_name);
    }
    closedir(dp);
```
### Fungsi `removeFile()`
Membutuhkan 2 parameter, direktori file berada dan nama file itu sendiri. Keduanya digabung dan dijadikan argumen untuk bash command `rm`.
```c
void removeFile(char *dir, char *fileName) 
{
	char source[100];
	strcpy(source, dir);
	strcat(source, fileName);

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", source, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
## E. Membuat List Hewan Air
```c
    writeToTxt(textFile, pathAir);
```
### Fungsi `writeToTxt()`
Menggabungkan parameter nama file dan folder tujuan menjadi path file txt yang akan di-write. Membuka folder lalu traverse file-file. Jika file adalah png atau jpg akan menulis nama hewan sesuai format (dengan fungsi `getDetails()`) kedalam file list.txt.
```c
void writeToTxt(char *fileName, char *dest) 
{
	char filePath[100];
	strcpy(filePath, dest);
	strcat(filePath, fileName);

	FILE *fp = fopen(filePath, "w");
	
	if (fp == NULL)
		printf("error opening file\n");
	
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(dest);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "jpg") || 
    		findPattern(dirp->d_name, "png"))
	    	fprintf(fp, "%s\n", getDetails(dest, dirp->d_name));
    }
    closedir(dp);
    
    fclose(fp);
}

```
### Fungsi `getDetails()`
Membutuhkan parameter folder yang menampung file dan nama file itu sendiri. Mengembalikan string berupa nama dan detail file sesuai format UID_[UID file permission]_Nama File.[jpg/png] (misal: conan_rwx_hewan.png) dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

Mencari UID dengan bantuan library `sys/stat.h`, dan `pwd.h`, menggunakan struct `passwd` dan `getpwuid()`. UID file permission dapat didapat dengan bantuan `sys/stat.h`, mengecek apakah memiliki permission untuk read/write/execute dengan fs.st_mode dan S_I[R|W|X]USR.
```c
const char* getDetails(char *dir, char *fileName) 
{
	char *res;
	res = malloc(sizeof(char)*100);
	int r, s;
	
	char filePath[100];
	strcpy(filePath, dir);
	strcat(filePath, fileName);
	
	struct stat info;
	r = stat(filePath, &info);
		
	struct passwd *pw = getpwuid(info.st_uid);
	
	if (pw != 0) strcpy(res, pw->pw_name);
	
	strcat(res, "_");
	
	struct stat fs;
	s = stat(filePath, &fs);

	if (fs.st_mode & S_IRUSR) 
		strcat(res, "r");
	if (fs.st_mode & S_IWUSR) 
		strcat(res, "w");
	if (fs.st_mode & S_IXUSR) 
		strcat(res, "x");
		
	strcat(res, "_");
	
	strcat(res, fileName);
	
	return res;
}
```
## Hasil Run

![run program](img/3.jpg)
![file](img/3,file.jpg)
![file drakor](img/3.air.jpg)
![action](img/3.darat.jpg)


# Source Code Soal 3
```c
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <pwd.h>

void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void unzip(char *zipFile, char *dest) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"unzip", "-d", dest, zipFile, NULL};
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
	}
}


int findPattern(char *str, char *pat) 
{
	return (strstr(str, pat) != NULL);
}


void move(char *src, char *fileName, char *dest) 
{
	char source[100];
	strcpy(source, src);
	strcat(source, fileName);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeDir(char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", "-r", dir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeFile(char *dir, char *fileName) 
{
	char source[100];
	strcpy(source, dir);
	strcat(source, fileName);

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", source, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

const char* getDetails(char *dir, char *fileName) 
{
	char *res;
	res = malloc(sizeof(char)*100);
	int r, s;
	
	char filePath[100];
	strcpy(filePath, dir);
	strcat(filePath, fileName);
	
	struct stat info;
	r = stat(filePath, &info);
		
	struct passwd *pw = getpwuid(info.st_uid);
	
	if (pw != 0) strcpy(res, pw->pw_name);
	
	strcat(res, "_");
	
	struct stat fs;
	s = stat(filePath, &fs);

	if (fs.st_mode & S_IRUSR) 
		strcat(res, "r");
	if (fs.st_mode & S_IWUSR) 
		strcat(res, "w");
	if (fs.st_mode & S_IXUSR) 
		strcat(res, "x");
		
	strcat(res, "_");
	
	strcat(res, fileName);
	
	return res;
}

void writeToTxt(char *fileName, char *dest) 
{
	char filePath[100];
	strcpy(filePath, dest);
	strcat(filePath, fileName);

	FILE *fp = fopen(filePath, "w");
	
	if (fp == NULL)
		printf("error opening file\n");
	
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(dest);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "jpg") || 
    		findPattern(dirp->d_name, "png"))
	    	fprintf(fp, "%s\n", getDetails(dest, dirp->d_name));
    }
    closedir(dp);
    
    fclose(fp);
}

int main() 
{
	char* pathDarat = "/home/doanda/modul2/darat/";
	char* pathAir = "/home/doanda/modul2/air/";
	char* pathAnimal = "/home/doanda/modul2/animal/";
	char* pathModul = "/home/doanda/modul2/";
	char* zipFile = "animal.zip";
	char* textFile = "list.txt";
	
	makeDir(pathDarat);
	sleep(3);
	makeDir(pathAir);
	
	unzip(zipFile, pathModul);
	
	DIR *dp;
	struct dirent *dirp;
	
    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "darat")) {
    		move(pathAnimal, dirp->d_name, pathDarat);
    	}
    }
    closedir(dp);
    
    sleep(3);

    dp = opendir(pathAnimal);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "air"))
    		move(pathAnimal, dirp->d_name, pathAir);
    }
    closedir(dp);
    
    removeDir(pathAnimal);
    
    dp = opendir(pathDarat);
    while ((dirp = readdir(dp)) != NULL) {
    	if (findPattern(dirp->d_name, "bird"))
    		removeFile(pathDarat, dirp->d_name);
    }
    closedir(dp);
    
    writeToTxt(textFile, pathAir);
    
	return 0;
}
```
