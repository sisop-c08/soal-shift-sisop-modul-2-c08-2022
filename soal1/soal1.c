#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>

void download(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/wget", command);
    } 
    else{
		while(wait(&status) > 0);
    }
}

void makedir(char *command[])
{
    pid_t child_id;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/mkdir", command);
    } 
    else{
    int status;
		while(wait(&status) > 0);
    }
    
}

// makedir versi 2
void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void unzip(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();

	if (child_id == 0) {
		execv("/bin/unzip", command);
    }
    else {
		while(wait(&status) > 0);
    }
}

void remove_file(char *command[])
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0){
        execv("/bin/rm", command);
    } 
    else {
		while(wait(&status) > 0);
    }
}

void zip(char *name, char *password) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
    	char zipName[100];
    	sprintf(zipName, "%s.zip %s/", name, name);
		char *argv[] = {"zip", "--password", password, "-r", zipName, NULL};

    	execv("/bin/zip", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}

void mvRename(char *oldName, char *newName) 
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id == 0) {
		char *argv[] = {"mv", oldName, newName, NULL};
		execv("/bin/mv", argv);
    }
    else {
		while(wait(&status) > 0);
    }
}

char* getTime() 
{
	int h, m, s;
	char *curtime = malloc(sizeof(char)*50);

	time_t now = time(NULL);
	struct tm *local = localtime(&now);

	h = local->tm_hour;
	m = local->tm_min;
	s = local->tm_sec;

	sprintf(curtime, "%02d:%02d:%02d", h, m, s);
	
	return curtime;
}

// stub
char* pullChar() 
{
	return "5_childe";
}

// stub
char* pullWeapon() 
{
	return "5_bigboysword";
}

void outputTxt(char *dir, int *primo, int *count) 
{
	// get current time in hh:mm:ss format
	char curtime[50];
	strcpy(curtime, getTime());

	// open txt (dummy name)
	char dummyPath[100];
	sprintf(dummyPath, "%s/dummy", dir);

	FILE *fp = fopen(dummyPath, "w");

	if (fp == NULL)
		printf("error opening file\n");

	for (int i = 0; i < 10; i++) 
	{
		if (*primo < 160) break;

		char hasilGacha[100];
		*count += 1;
		*primo -= 160;

		if (i % 2 == 0) {
			char *charData = pullChar(); // get string rarity_name
			sprintf(hasilGacha, "%d_characters_%s_%d", *count, charData, *primo);
		}
		else if (i % 2 != 0) {
			char *weaponData = pullWeapon(); // get string rarity_name
			sprintf(hasilGacha, "%d_weapons_%s_%d", *count, weaponData, *primo);
		}
		
		fprintf(fp, "%s\n", hasilGacha);
	}

	fclose(fp);
	
	// rename txt file
	char filePath[100];
	sprintf(filePath, "%s/%s_gacha_%d", dir, curtime, *count);
	mvRename(dummyPath, filePath);
}

void gacha() {
	int primogems = 20000;
	int gcount = 0;
	
	char dummyPath[50] = "gacha_gacha/dummy";
	char folderPath[50];
	
	while (primogems >= 160) {
		if (gcount % 9 == 0) {
			if (gcount != 0) {
				sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
				mvRename(dummyPath, folderPath);
			}
			makeDir(dummyPath); // wait
		}
			
		outputTxt(dummyPath, &primogems, &gcount);
		sleep(1);
	}

	sprintf(folderPath, "gacha_gacha/total_gacha_%d", gcount);
	mvRename(dummyPath, folderPath);
}


int main(){

    char *argsdownload1[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-O","char.zip",NULL};
    char *argsdownload2[] = {"wget","-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-O", "weapons.zip",NULL};
    char *argsunzip1[] = {"unzip","char.zip",NULL};
    char *argsunzip2[] = {"unzip","weapons.zip",NULL};
    char *argsrmv1[] = {"rm","char.zip",NULL};
    char *argsrmv2[] = {"rm","weapons.zip",NULL};
    char *argmkdir[] = {"mkdir", "-p", "gacha_gacha",NULL};

    //download(argsdownload1);
    printf("char downloaded\n");
    //download(argsdownload2);
    printf("weapons downloaded\n");

    sleep(1);

    unzip(argsunzip1);
    printf("unzip char\n");
    unzip(argsunzip2);
    printf("unzip weapons\n");
    
    sleep(1);
    makedir(argmkdir);
    printf("makedir gacha\n");
    
	// gacha
    gacha();
    
    // zip gacha_gacha "not_safe_for_wibu", masi pake system
    system("zip -r --password pass not_safe_for_wibu.zip gacha_gacha/*");
    
    // versi bukan system, tapi masi eror
    char *dirName = "gacha_gacha";
    char *password = "satuduatiga";
    char *zipName = "not_safe_for_wibu.zip";
	// zip terus rename 
    // zip(dirName, password);
    // mvRename(dirName, zipName); 
    
    // delete chars weapons (dir/zip)
    //remove_file(argsrmv1);
    //printf("char removed\n");
    //remove_file(argsrmv2);
    //printf("weapons removed\n");

    // delete gacha_gacha dir

    
    return 0;

}
